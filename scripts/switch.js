// this seems like a bad idea but it's fine for now. -M

// use light theme by default
let light = true

function themeSwitch(store, attributes, toggle) {
    // if light is true, set elements to black and vice-versa for white
    let colorChange = light ? "black" : "white"

    // set body color regardless of document
    // background-color style is changed through .setAttribute since .classList.add doesn't always work
    // ensure min-height: 100% is retained to remove whitespace
    document.body.setAttribute("style", `background-color: ${colorChange}; min-height: 100%;`);

    // attribute is expected to be an object in the format "elementClass": ["attributeName", "attributeParams"]
    for (const elementClass in attributes) {
        let elements = document.getElementsByClassName(elementClass);
        // for each element found, set its attributeName followed by its attributeParameters
        for (let i = 0; i < elements.length; i++) {
            const attributeName = attributes[elementClass][0]
            // add colorChange in this script, since it is buggy to have it accessible outside of themeSwitch()
            const attributeParams = attributes[elementClass][1].replace("$COLOR", colorChange)
            elements[i].setAttribute(attributeName, attributeParams)
        }
    }

    // toggle is expected to be an object in the format "classToToggle": ["elementClass1", "elementClass2", ...]
    for (const property in toggle) {
        // forEach every class in the array
        toggle[property].forEach(arrayItem => {
            // get each element belonging to every class
            let elements = document.getElementsByClassName(arrayItem)
            // toggle the initial "classToToggle" name on each of the elements
            for (let i = 0; i < elements.length; i++) {
                elements[i].classList.toggle(property)
            }
        });
    }

    // invert cookie if theme button is clicked
    if (store) {
        document.cookie = `light=${!light};SameSite=Strict`
        light = !light
    }
}